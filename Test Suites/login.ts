<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>8ffd2ba0-ded8-4665-a714-5740ee3d0de7</testSuiteGuid>
   <testCaseLink>
      <guid>4dc34afa-23c2-4eae-82a8-eb6a65846bf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginValidInputWithDataDriven</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e63d5015-b527-42ec-af04-b8fa7118c5cd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/usernameAndPassword</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>e63d5015-b527-42ec-af04-b8fa7118c5cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>248a33ed-7272-4522-9d29-1141294651eb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e63d5015-b527-42ec-af04-b8fa7118c5cd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>03857178-397d-4247-b2bf-e96a3047a282</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>48c87942-4338-4af8-9e13-b7ec1cf8a2a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginAndLogout</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>91e581f4-92ce-4082-848d-1f23989aaf47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginProblemUserHardcode</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a95836a6-cca1-4533-adfb-ae515228aa9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginUserHasBannedWithAsUsual</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>be2e098d-0b7a-48cb-b7e7-19e4e0a934b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/loginValidInputWithAsUsual</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
