<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>checkout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>0d0aad98-4db2-4255-94f7-cbf80b1ee8f4</testSuiteGuid>
   <testCaseLink>
      <guid>35afd53b-94ad-470d-81f5-1a0febd18c12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CheckoutItemOnCartWithDataDriven</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4db1ba7a-ec5a-4de9-884a-31bc8e1b780c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/fullData</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>4db1ba7a-ec5a-4de9-884a-31bc8e1b780c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>firstname</value>
         <variableId>682b6007-3248-48d8-b2cc-cb14ac90d5b4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4db1ba7a-ec5a-4de9-884a-31bc8e1b780c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>lastname</value>
         <variableId>d844c43f-a7f3-4907-82d4-8ae5d1765676</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4db1ba7a-ec5a-4de9-884a-31bc8e1b780c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>zip</value>
         <variableId>04ba6fbe-044f-4921-8700-db88d27bb56d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4db1ba7a-ec5a-4de9-884a-31bc8e1b780c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>ae4ecc32-8796-4073-abc9-489a6ac08f48</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4db1ba7a-ec5a-4de9-884a-31bc8e1b780c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>ea855c5a-153e-449b-ba3d-6f98c35b9579</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>76afb0e3-fe11-4bef-8988-17db03ba4c7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CheckoutWithDataDriven2ItemOnCart</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>17a9bbc4-4feb-4c6c-8fb6-5ff926ce61c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/fullData</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>17a9bbc4-4feb-4c6c-8fb6-5ff926ce61c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>3c287f4c-d928-45dd-9a4a-75ea0f25c12d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>17a9bbc4-4feb-4c6c-8fb6-5ff926ce61c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>44dec0a4-686f-4063-91c1-d43eede9cbfc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>17a9bbc4-4feb-4c6c-8fb6-5ff926ce61c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>firstname</value>
         <variableId>54360793-d5cd-4a04-a1c7-bae1b5d1dabf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>17a9bbc4-4feb-4c6c-8fb6-5ff926ce61c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>lastname</value>
         <variableId>7acc7108-82b4-41c9-ad2b-b3c8c4b12772</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>17a9bbc4-4feb-4c6c-8fb6-5ff926ce61c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>zip</value>
         <variableId>5fac5ff8-2024-4295-ab6d-8dcf6f28ff4a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b236001d-3a10-4627-9dda-66e7943c1228</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CheckoutWithGlitchUser</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9dc80394-379b-4669-b978-329ebe198341</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/checkoutWithGlitchUser2ItemOnCart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>57c681b2-2851-49e8-8c5c-9f7f758cea84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/checkoutWithProblemUser</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
