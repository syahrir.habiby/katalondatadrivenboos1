import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.setText(findTestObject('loginPage/sauceLabs/username'), username)

WebUI.setText(findTestObject('loginPage/sauceLabs/password'), password)

WebUI.click(findTestObject('loginPage/sauceLabs/loginButton'))

WebUI.verifyElementVisible(findTestObject('loginPage/sauceLabs/pageSwagLabs'))

WebUI.click(findTestObject('checkoutPayment/buttonAddToCart'))

WebUI.click(findTestObject('checkoutPayment/Page_Swag Labs/buttonAddToCartBike'))

WebUI.click(findTestObject('checkoutPayment/cartLogo'))

WebUI.click(findTestObject('checkoutPayment/buttonCheckout'))

WebUI.setText(findTestObject('checkoutPayment/inputFirstName'), firstname)

WebUI.setText(findTestObject('checkoutPayment/inputLastName'), lastname)

WebUI.setText(findTestObject('checkoutPayment/inputZip'), zip)

WebUI.click(findTestObject('checkoutPayment/continueButton'))

WebUI.verifyElementVisible(findTestObject('checkoutPayment/paymentInformation'))

WebUI.verifyElementVisible(findTestObject('checkoutPayment/shippingInformation'))

WebUI.verifyElementVisible(findTestObject('checkoutPayment/itemTotal'))

WebUI.verifyElementVisible(findTestObject('checkoutPayment/tax'))

WebUI.verifyElementVisible(findTestObject('checkoutPayment/total'))

WebUI.click(findTestObject('checkoutPayment/buttonFinish'))

WebUI.verifyElementVisible(findTestObject('checkoutPayment/checkoutComplete'))

WebUI.click(findTestObject('checkoutPayment/buttonBackHome'))

WebUI.verifyElementVisible(findTestObject('loginPage/sauceLabs/pageSwagLabs'))

